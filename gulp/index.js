 'use strict';
var gulp  = require('gulp'),
	fs      = require('fs'),
	path    = require('path'),
	tasks   = fs.readdirSync('./gulp/tasks/'),
	plugins = require('gulp-load-plugins')();

tasks.forEach(function (task) {
	if (task !== '.DS_Store') {
		require(path.join(__dirname, 'tasks', task));
	}
});
