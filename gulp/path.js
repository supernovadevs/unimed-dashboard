'use strict';
module.exports = {

	source: {
		js: './src/js/**/*.js',
		sass: 'src/**/*.scss',
		jade: './src/**/*.jade',
		fonts: './src/fonts/**/*',
		img: './src/img/**/**/*.{jpg,png,gif}'
	},

	browserSync: {
		img: '.tmp/img/**/*',
		js: '.tmp/js/**/*.js',
		html: '.tmp/**/*.html',
		css: '.tmp/css/**/*.css'
	},

	server: {
		html: '.tmp/',
		js: '.tmp/js',
		img: '.tmp/img',
		css: '.tmp/css',
		fonts: '.tmp/fonts/',
		index: '.tmp/index.html'
	},

	build: {
		html: 'dist/',
		js: 'dist/js',
		img: 'dist/img',
		css: 'dist/css/',
		fonts: 'dist/fonts/'
	}

};
