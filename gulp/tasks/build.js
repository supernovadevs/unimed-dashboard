'use strict';
var gulp = require('gulp');

// Default task
module.exports = gulp.task('build', [
	'jade',
	'sass',
	'imagemin',
	'scripts',
	'inject-vendor',
	'copy-vendor',
	'browser-sync-server',
	'watch'
]);
