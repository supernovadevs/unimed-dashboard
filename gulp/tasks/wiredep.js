// Necessary Plugins
var gulp  = require('gulp'),
	paths   = require('../path'),
	wiredep = require('wiredep').stream,
	plugins = require('gulp-load-plugins')(),
	env     = require('minimist')(process.argv.slice(2));


module.exports = gulp.task('inject-vendor', function () {
	gulp.src(paths.server.index)
		.pipe(wiredep({}))
		.pipe(gulp.dest(paths.server.html));
});
