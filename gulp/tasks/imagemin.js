'use strict';

// Necessary Plugins
var gulp   = require('gulp'),
  gulpif   = require('gulp-if'),
	paths    = require('../path'),
	plumber  = require('gulp-plumber'),
	imagemin = require('gulp-imagemin'),
  env      = require('minimist')(process.argv.slice(2));

// Minify Images
module.exports = gulp.task('imagemin', function () {
	return gulp.src(paths.source.img)
		.pipe(plumber())
		.pipe(imagemin({
			optimizationLevel: 5,
			progressive: true,
			interlaced: true
		}))
    .pipe(gulpif(env.p, gulp.dest(paths.build.img)))
		.pipe(gulp.dest(paths.server.img));
});
