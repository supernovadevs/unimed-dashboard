'use strict';

// Necessary Plugins
var gulp = require('gulp'),
	paths = require('../path'),
	plugins = require('gulp-load-plugins')();

module.exports = gulp.task('fixjs', function () {
	return gulp.src(paths.source.js)
		.pipe(plugins.fixmyjs())
		.pipe(gulp.dest('./src'));
});

module.exports = gulp.task('copy-vendor', function () {
	return gulp.src('.tmp/vendor/**/*')
		.pipe(gulp.dest('dist/vendor'));
});
