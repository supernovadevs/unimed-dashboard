'use strict';

// Necessary Plugins
var gulp      = require('gulp'),
	paths       = require('../path'),
	gulpif      = require('gulp-if'),
	browserSync = require('browser-sync');

// Serve files from /tmp/
module.exports = gulp.task('browser-sync', function () {

	var files = [
		paths.browserSync.js,
		paths.browserSync.img,
		paths.browserSync.css,
		paths.browserSync.html
	];

	browserSync.init(files, {
		notify: false,
		injectChanges: true,
		server: {
			baseDir: paths.server.html,
		}
	});
});


module.exports = gulp.task('browser-sync-server', function () {

	var files = [
		paths.browserSync.js,
		paths.browserSync.img,
		paths.browserSync.css,
		paths.browserSync.html
	];

	browserSync.init(files, {
		notify: false,
		injectChanges: true,
		server: {
			baseDir: paths.build.html,
		}
	});
});
