'use strict';
// Necessary Plugins
var gulp  = require('gulp'),
	paths   = require('../path'),
	plugins = require('gulp-load-plugins')();

var verssions ="last 1 Chrome version', 'last 3 iOS versions', 'last 3 Android versions'";

// Compile alto prefix for browser
module.exports = gulp.task('autoprefixer', function () {
	return gulp.src(paths.build.css)
		.pipe(plugins.plumber())
		.pipe(plugins.autoprefixer(versions))
		.pipe(gulp.dest(paths.build.css))
});
