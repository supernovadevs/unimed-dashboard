'use strict';
var gulp = require('gulp');

// Default task
module.exports = gulp.task('default', ['jade','sass','imagemin','scripts','inject-vendor','browser-sync','watch']);
