'use strict';

// Necessary Plugins
var gulp      = require('gulp'),
	gulpif      = require('gulp-if'),
	paths       = require('../path'),
	browserSync = require('browser-sync'),
	purify      = require('gulp-purifycss'),
	plugins     = require('gulp-load-plugins')(),
	env         = require('minimist')(process.argv.slice(2));

var serverPaths = [
	'.tmp/**/*.js',
	'.tmp/**/*.html',
	'.tmp/components/**/*.html'
];

var distPaths = [
	'dist/**/*.js',
	'dist/**/*.html',
	'dist/components/**/*.html'
]

module.exports = gulp.task('sass', function () {
	gulp.src('src/sass/main.scss')
		.pipe(plugins.plumber())
		.pipe(plugins.sass({errLogToConsole: true}))
		.pipe(gulpif(env.p, plugins.minifyCss()))
    .pipe(gulp.dest(paths.build.css))
    .pipe(gulpif(env.p, gulp.dest(paths.build.css)))
		.pipe(gulp.dest(paths.server.css))
		.pipe(browserSync.stream());
})
