
'use strict';
angular.module('unimed', ['core']).config(function($stateProvider, $urlRouterProvider, $httpProvider, unsavedWarningsConfigProvider) {
	$httpProvider.interceptors.push('httpInterceptor');
	$urlRouterProvider.otherwise("/login");
	unsavedWarningsConfigProvider.navigateMessage = "Você deseja sair da tela? Qualquer informação não salva pode ser perdida.";
	unsavedWarningsConfigProvider.logEnabled = true;
	unsavedWarningsConfigProvider.reloadMessage = "Custom Reload Message";
	unsavedWarningsConfigProvider.routeEvent = '$stateChangeStart';
}).run([
	'$rootScope',
	'$state',
	'$stateParams',
	function($rootScope, $state, $stateParams) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
	}
]).directive("mwConfirmClick", [function() {
		return {
			priority: -1,
			restrict: 'A',
			scope: {
				confirmFunction: "&mwConfirmClick"
			},
			link: function(scope, element, attrs) {
				element.bind('click', function(e) {
					var message = attrs.mwConfirmClickMessage
						? attrs.mwConfirmClickMessage
						: "Tem certeza que deseja realizar essa ação?";
					if (confirm(message)) {
						scope.confirmFunction();
					}
				});
			}
		}
	}
]).filter('percentage', [
	'$filter',
	function($filter) {
		return function(input, decimals) {
			var value = 0 + '%';
			if (input) {
				value = $filter('number')(input * 100, decimals) + '%';
			}
			return value;
		};
	}
])
