'use strict';

angular
  .module('statistics')
  .factory('statisticsFactory', statisticsFactory);

function statisticsFactory($http, $q, API_ENDPOINT) {

  var service = {
    getStats: getStats
  };

  return service;

  function getStats(id) {
    var dfd = $q.defer();
    $http.post(API_ENDPOINT.requestAPI + 'LogEventRequest', {
      "@class": ".LogEventRequest",
      "eventKey": 'CalculateQuestionsStats',
      "playerId": "58309640a1712a04a8cd913f"
    }
    ).then(function(response) {
      dfd.resolve(response);
    }, function(error) {
      dfd.reject(error);
    })
    return dfd.promise;
  }

}
