'use strict';
angular
  .module('statistics')
  .config(function($stateProvider) {
    $stateProvider
      .state('dashboard.statistics', {
        data : { pageTitle: 'Estatísticas' },
        url: "/statistics",
        views: {
          'menuContent': {
            controller: 'statisticsController as vm',
            templateUrl: "js/components/statistics/views/index.html"
          }
        }
      })
  });
