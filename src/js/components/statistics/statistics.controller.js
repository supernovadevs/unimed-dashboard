(function() {
  'use strict';

  angular
    .module('statistics', [])
    .controller('statisticsController', statisticsController);

  function statisticsController(statisticsFactory) {
    var vm = this;
    vm.name = 'vitor';
    vm.id = localStorage.getItem('_id');
    vm.averageQuestionValue;
    vm.correctAnswersPercentByTheme;
    vm.numberQuestionsByTheme;
    vm.sort = {
    	type:'name',
    	reverse: false,
    }
    vm.themes = [
      { name: "1 - Identidade Organizacional" },
      { name: "2 - Mapa Estratégico" },
      { name: '3 - ANS' },
      { name: '4 - Atendimento ao Cliente' },
      { name: "5 - Viver Bem" },
      { name: "6 - Serviços Unimed" },
      { name: "7 - Perfil Unimed" },
      { name: "8 - RH com Você" },
      { name: "9 - Governança Corporativa" }
    ]


    function load() {
      statisticsFactory.getStats(vm.id).then(
        function(response) {
        	console.log(response)
          vm.themesMapped = vm.themes.map(function(item,i){
            item.correctAnswersPercent = response.data.scriptData.correctAnswersPercentByThemeArray[i]
            item.numberQuestions = response.data.scriptData.numberQuestionsByThemeArray[i]
            return item;
          })
          console.log(vm.themesMapped);
          vm.averageQuestionValue = response.data.scriptData.averageQuestionValue;
        },
        function(error){
        	console.log(error);
        }
      )
    }
    load();
  }

})();
