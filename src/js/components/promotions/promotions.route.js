'use strict';
angular
  .module('promotions')
  .config(function($stateProvider) {
    $stateProvider
      .state('dashboard.promotions', {
        data : { pageTitle: 'Promoções' },
        url: "/promotions",
        views: {
          'menuContent': {
            controller: 'PromotionsController as vm',
            templateUrl: "js/components/promotions/views/index.html"
          }
        }
      })
      .state('dashboard.promotionsCreate', {
        data : { pageTitle: 'Promoções' },
				url: "/promotions/new",
				views: {
					'menuContent': {
						controller: 'PromotionsController as vm',
						templateUrl: "js/components/promotions/views/create.html"
					}
				}
			})
			.state('dashboard.promotionsEdit', {
        data : { pageTitle: 'Promoções' },
				url: "/promotions/edit/{id}",
				params:{
					promotion:null,
					index:null
				},
				views: {
					'menuContent': {
						controller: 'PromotionsController as vm',
						templateUrl: "js/components/promotions/views/edit.html"
					}
				}
			})
			.state('dashboard.promotionsShow', {
        data : { pageTitle: 'Promoções' },
				url: "/promotions/show/{id}",
				params:{
					promotion:null
				},
				views: {
					'menuContent': {
						controller: 'PromotionsController as vm',
						templateUrl: "js/components/promotions/views/show.html"
					}
				}
			})
  });
