'use strict';

angular
  .module('promotions',[])
  .controller('PromotionsController', PromotionsController);

function PromotionsController(Promotionsfactory,DialogFactory,$state,$stateParams) {
  var vm = this;
  vm.bonusList = [];
  vm.sort = {
    type:'startDates._d',
    reverse: true,
  }

  angular.extend(vm, {
    save:save,
    update:update,
    destroy:destroy,
    loadAll:loadAll,
    loadBonus:loadBonus
  });

  function loadAll() {
    Promotionsfactory.getAllBonus().then(
      function(response){
        console.log(response);
        vm.bonusList = response.data.map(function(item){
          item.startDates = moment(item.startDate, "DD-MM-YYYY");
          item.endDates = moment(item.endDate, "DD-MM-YYYY");
          return item;
        })

      },
      function(error){
        console.log(error);
      }
    )
  }

  function save(form, bonus) {
    if (form.$valid) {
      Promotionsfactory.saveBonus(bonus).then(
        function(response) {
          DialogFactory.showSuccessDialog('Promoção Cadastrada com sucesso');
          $state.go('dashboard.promotions')
        },
        function(error) {
          DialogFactory.showErrorDialog('Erro ao cadastrar promoção');
        }
      )
    } else {
      DialogFactory.showErrorDialog('Existem erros no preenchimento do formulário');
    }
  }

  function update(form, bonus) {

    if (form.$valid) {
      Promotionsfactory.updateBonus(bonus).then(
        function(response) {
          DialogFactory.showSuccessDialog('Promoção Atualizada com sucesso');
          $state.go('dashboard.promotions')
        },
        function(error) {
          DialogFactory.showErrorDialog('Error ao editar Promoção');
        }
      )
    } else {
      DialogFactory.showErrorDialog('Existem erros no preenchimento do formulário');
    }
  }


  function loadBonus() {
    if ($stateParams.promotion) {
      vm.bonus = $stateParams.promotion;
    } else {
      var id = { "$oid": $stateParams.id }
      Promotionsfactory.getBonus(id).then(
        function(response) {
          console.log(response.data);
          vm.bonus = response.data[0];
        },
        function(error) {
          console.log(erro);
        }
      )
    }
  }

  function destroy(id) {
    Promotionsfactory.deleteBonus(id).then(
      function(response) {
        DialogFactory.showSuccessDialog('Promoção Removida com sucesso');
        loadAll();
      },
      function(error) {
        DialogFactory.showErrorDialog('Erro ao Remover promoção');
      }
    )
  }

}
