'use strict';
angular
  .module('promotions')
  .factory('Promotionsfactory', Promotionsfactory);

function Promotionsfactory($q,$http,$httpParamSerializer,API_ENDPOINT) {
  var service = {
    getBonus:getBonus,
    saveBonus:saveBonus,
    deleteBonus:deleteBonus,
    updateBonus:updateBonus,
    getAllBonus: getAllBonus
  };

  return service;

  function getAllBonus() {
		var dfd = $q.defer();
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.bonusperiod/find',
			data: {query:{}},
			headers: {
				'Content-Type': 'application/json'
			}
		}
    
		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})
		return dfd.promise;
	}


  function saveBonus(bonus){
		var dfd = $q.defer();
		var bonus = bonus;
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.bonusperiod/insert',
			data: bonus,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}


  function updateBonus(bonus) {
		var dfd = $q.defer();
		var queryContent = {
			"_id": bonus._id
		};
		var bonusQuery = {query: queryContent, update: bonus, upsert: false, multi:false};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.bonusperiod/update',
			data: bonusQuery,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}


  function getBonus(id) {
		var dfd = $q.defer();
		var bonus = {query: {"_id": id}};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.bonusperiod/find',
			data: bonus,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

  function deleteBonus(bonus) {
		var dfd = $q.defer();
		var bonus = {query: {"_id": bonus._id}};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.bonusperiod/remove',
			data: bonus,
			headers: {
				'Content-Type': 'application/json'
			}
		}
		$http(configRequest).then(function(response) {
			dfd.resolve(response)
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

}
