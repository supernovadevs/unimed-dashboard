'use strict';
angular
  .module('admins')
  .config(function($stateProvider) {
    $stateProvider
      .state('dashboard.admins', {
        data : { pageTitle: 'Administradores' },
        url: "/admins",
        views: {
          'menuContent': {
            controller: 'adminsController as vm',
            templateUrl: "js/components/admins/views/index.html"
          }
        }
      })
      .state('dashboard.adminsCreate', {
        data : { pageTitle: 'Administradores' },
        url: "/admins/new",
        views: {
          'menuContent': {
            controller: 'adminsController as vm',
            templateUrl: "js/components/admins/views/create.html"
          }
        }
      })
      .state('dashboard.adminsShow', {
        data : { pageTitle: 'Administradores' },
        url: "/admins/show/{id}",
        params: {
          admin: null
        },
        views: {
          'menuContent': {
            controller: 'adminsController as vm',
            templateUrl: "js/components/admins/views/show.html"
          }
        }
      })
  });
