(function() {
  'use strict';

  angular
    .module('admins', [])
    .controller('adminsController', adminsController);

  function adminsController(Adminsfactory, DialogFactory, $state, $stateParams) {
    var vm = this;
    vm.adminList = [];
    vm.currentUser = localStorage.getItem('_user');

    angular.extend(vm, {
      save: save,
      destroy: destroy,
      loadAll: loadAll,
      loadAdmin: loadAdmin,
    });

    function loadAll() {
      Adminsfactory.getAllAdmins().then(
        function(response) {
          vm.adminList = response.data
        },
        function(error) {
          console.log(error);
        }
      )
    }

    function save(form, admin) {
      if (form.$valid) {
        admin.createdBy = vm.currentUser;
        Adminsfactory.saveAdmins(admin).then(
          function(response) {
            DialogFactory.showSuccessDialog('Adiministrador cadastrado com sucesso');
            $state.go('dashboard.admins')
          },
          function(error) {
            DialogFactory.showErrorDialog('Erro ao cadastrar Adiministrador');
          }
        )
      } else {
        DialogFactory.showErrorDialog('Existem erros no preenchimento do formulário');
      }
    }

    function loadAdmin() {
      if ($stateParams.admin) {
        vm.admin = $stateParams.admin;
      } else {
        var id = { "$oid": $stateParams.id }
        Adminsfactory.getAdmin(id).then(
          function(response) {
            vm.admin = response.data[0];
          },
          function(error) {
            console.log(erro);
          }
        )
      }
    }

    function destroy(id) {
      Adminsfactory.deleteAdmins(id).then(
        function(response) {
          DialogFactory.showSuccessDialog('Adiministrador Removido com sucesso');
          loadAll();
        },
        function(error) {
          DialogFactory.showErrorDialog('Erro ao Remover Adiministrador');
        }
      )
    }

  }
})();
