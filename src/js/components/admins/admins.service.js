'use strict';
angular
  .module('admins')
  .factory('Adminsfactory', Adminsfactory);

function Adminsfactory($q, $http, $httpParamSerializer, API_ENDPOINT) {
  var service = {
    getAdmin: getAdmin,
    saveAdmins: saveAdmins,
    deleteAdmins: deleteAdmins,
    getAllAdmins: getAllAdmins
  };

  return service;

  function getAllAdmins() {
    var dfd = $q.defer();
    var configRequest = {
      method: 'POST',
      url: API_ENDPOINT.host + 'script.admin/find',
      data: {"query":{}},
      headers: {
        'Content-Type': 'application/json'
      }
    }
    
    $http(configRequest).then(function(response) {
      dfd.resolve(response);
    }, function(error) {
      dfd.reject(error);
    })
    return dfd.promise;
  }


  function saveAdmins(admin) {
    var dfd = $q.defer();
    var configRequest = {
      method: 'POST',
      url: API_ENDPOINT.host + 'script.admin/insert',
      data: admin,
      headers: {
        'Content-Type': 'application/json'
      }
    }

    $http(configRequest).then(function(response) {
      dfd.resolve(response);
    }, function(error) {
      dfd.reject(error);
    })

    return dfd.promise;
  }

  function getAdmin(id) {
    var dfd = $q.defer();
    var admins = { query: {"_id": id} };
    var configRequest = {
      method: 'POST',
      url: API_ENDPOINT.host + 'script.admin/find',
      data: admins,
      headers: {
        'Content-Type': 'application/json'
      }
    }

    $http(configRequest).then(function(response) {
      dfd.resolve(response);
    }, function(error) {
      dfd.reject(error);
    })

    return dfd.promise;
  }

  function deleteAdmins(admin) {
    var dfd = $q.defer();
    var admin = {query: {"_id": admin._id}};
    var configRequest = {
      method: 'POST',
      url: API_ENDPOINT.host + 'script.admin/remove',
      data: admin,
      headers: {
        'Content-Type': 'application/json'
      }
    }
    $http(configRequest).then(function(response) {
      dfd.resolve(response)
    }, function(error) {
      dfd.reject(error);
    })

    return dfd.promise;
  }

}
