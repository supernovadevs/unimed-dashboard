(function() {
  'use strict';
  angular.module('questions', []).controller('QuestionsController', QuestionsController);

  function QuestionsController($scope, $state, $stateParams, $timeout, DialogFactory, ApiFactory, $filter, $window) {
    var vm = this;
    vm.loadAll = loadAll;
    vm.save = save;
    vm.questions = []
    vm.question = {};
    vm.question.choices = [{ item: "a" }, { item: "b" }, { item: "c" }];
    vm.update = update;
    vm.destroy = destroy;
    vm.loadQuestion = loadQuestion;
    vm.initLocalObjects = initLocalObjects;
    vm.localQuestion = {};
    vm.currentUser = localStorage.getItem('_user');
    vm.currentId = localStorage.getItem('_id');
    vm.sort = {
      type: 'mapDate._d',
      reverse: true
    }
    vm.loadQuestionsWithString = loadQuestionsWithString;
    var startDate = moment().subtract(15, "d").format("DD-MM-YYYY")
    var endDate = moment().add(15, "d").format("DD-MM-YYYY");
    vm.date = {
      start : startDate,
      end : endDate
    };
    console.log(vm.date);


    function initLocalObjects() {
      vm.localQuestion.cenarios = [
        { name: "1 - Posto de Enfermagem" },
        { name: "2 - Atendimento ao Cliente" },
        { name: "3 - Escritório" },
        { name: "4 - Praça Luiza Távora" },
        { name: "5 - Consultório" },
      ]

      vm.localQuestion.choices = [
        { item: "a" }, { item: "b" }, { item: "c" }
      ]

      vm.localQuestion.characters = [
        { name: "1 - Sofia - RH" },
        { name: "2 - Arthur - Planejamento" },
        { name: "3 - Roberta - Atendimento ao Cliente" },
        { name: "4 - Ana - Enfermeira" },
        { name: "5 - Dr. José Carlos - Médico" }
      ]

      vm.localQuestion.themes = [
        { name: "1 - Identidade Organizacional" },
        { name: "2 - Mapa Estratégico" },
        { name: '3 - ANS' },
        { name: '4 - Atendimento ao Cliente' },
        { name: "5 - Viver Bem" },
        { name: "6 - Serviços Unimed" },
        { name: "7 - Perfil Unimed" },
        { name: "8 - RH com Você" },
        { name: "9 - Governança Corporativa" }
      ]
    }


    function loadQuestionsWithString() {
      ApiFactory.getQuestionWithText(vm.stringToFind).then(
        function(response){
          vm.questions = response.data;
          vm.mappedQuestions = vm.questions.map(calcItemPorcentage).map(function(item) {
            item.mapDate = moment(item.date, "DD-MM-YYYY");
            return item;
          })
        },
        function(error) {
          DialogFactory.showErrorDialog('Erro ao carregar as perguntas');
        }
      )
    }


    function loadAll() {
      ApiFactory.getAllQuestions(vm.date).then(
        function(response) {
          vm.questions = response.data;
          vm.mappedQuestions = vm.questions.map(calcItemPorcentage).map(function(item) {
            item.mapDate = moment(item.date, "DD-MM-YYYY");
            return item;
          })
        },
        function(error) {
          DialogFactory.showErrorDialog('Erro ao carregar as perguntas');
        }
      )
    }

    function calcItemPorcentage(item) {
      item.correctAnswersPercent = item.timesAnsweredCorrectly / (item.timesAnsweredCorrectly + item.timesAnsweredIncorrectly);
      if (isNaN(item.correctAnswersPercent)) {
        item.correctAnswersPercent = 0;
      }
      return item;
    }

    function loadQuestion() {
      if ($stateParams.question) {
        vm.question = $stateParams.question;
        initLocalObjects()
      } else {
        var id = { "$oid": $stateParams.id }
        ApiFactory.getquestion(id).then(
          function(response) {
            console.log(response.data);
            vm.question = response.data[0];
            initLocalObjects()
          },
          function(error) {
            console.log(erro);
          }
        )
      }
    }

    function save(form, question) {
      if(form.$valid) {
        question.timesAnsweredCorrectly = 0;
        question.timesAnsweredIncorrectly = 0;
        question.createdBy = vm.currentUser;
        question.formattedDate = moment(question.date, "DD-MM-YYYY").toISOString()
        ApiFactory.checkDailyQuestions(vm.currentId, question.date).then(
          function(response) {
            ApiFactory.saveQuestion(question).then(
              function(response) {
                DialogFactory.showSuccessDialog('Pergunta Cadastrada com sucesso');
                $state.go('dashboard.questions')
              },
              function(error) {
                DialogFactory.showErrorDialog('Erro ao cadastrar pergunta');
              }
            )
          },
          function(error) {
            DialogFactory.showErrorDialog(error);
          }
        )
      } else {
        DialogFactory.showErrorDialog('Existem erros no preenchimento do formulário');
      }
    }

    function update(form, question) {
      if (form.$valid) {
        question.formattedDate = moment(question.date, "DD-MM-YYYY").toISOString();
        ApiFactory.updateQuestion(question).then(
          function(response) {
            DialogFactory.showSuccessDialog('Pergunta Atualizada com sucesso');
            $state.go('dashboard.questions')
          },
          function(error) {
            DialogFactory.showErrorDialog('Error ao editar o formulário');
          }
        )
      } else {
        DialogFactory.showErrorDialog('Existem erros no preenchimento do formulário');
      }
    }

    function destroy(id) {
      ApiFactory.deleteQuestion(id).then(
        function(response) {
          DialogFactory.showSuccessDialog('Pergunta Removida com sucesso');
          loadAll();
        },
        function(error) {
          DialogFactory.showErrorDialog('Erro ao Remover Pergunta');
        }
      )
    }
  }
}());
