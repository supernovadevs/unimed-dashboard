'use strict';
angular
	.module('questions')
	.config(function ($stateProvider) {
		$stateProvider
			.state('dashboard.questions', {
				data : { pageTitle: 'Perguntas' },
				url: "/questions",
				views: {
					'menuContent': {
						controller: 'QuestionsController as vm',
						templateUrl: "js/components/questions/views/index.html"
					}
				}
			})
			.state('dashboard.questionsCreate', {
				data : { pageTitle: 'Perguntas' },
				url: "/questions/new",
				views: {
					'menuContent': {
						controller: 'QuestionsController as vm',
						templateUrl: "js/components/questions/views/create.html"
					}
				}
			})
			.state('dashboard.questionsEdit', {
				data : { pageTitle: 'Perguntas' },
				url: "/questions/edit/{id}",
				params:{
					question:null,
					index:null
				},
				views: {
					'menuContent': {
						controller: 'QuestionsController as vm',
						templateUrl: "js/components/questions/views/edit.html"
					}
				}
			})
			.state('dashboard.questionsShow', {
				data : { pageTitle: 'Perguntas' },
				url: "/questions/show/{id}",
				params:{
					question:null
				},
				views: {
					'menuContent': {
						controller: 'QuestionsController as vm',
						templateUrl: "js/components/questions/views/show.html"
					}
				}
			})
	});
