'use strict';
angular
  .module('login', [])
  .controller('LoginController', LoginController);

function LoginController($state, $stateParams, $base64, LoginFactory, DialogFactory, ApiFactory) {
  var vm = this;
  vm.user = {};

  angular.extend(vm, { login: login });

  function login(user) {
    if (vm.user.name && vm.user.password) {
      LoginFactory.loginAD(user)
        .then(LoginFactory.loginAuth)
        .then(LoginFactory.loginGS)
        .then(success)
        .catch(errorHandler);
    } else {
      DialogFactory.showErrorDialog('Usuário e senha requeridos');
    }
  }


  function errorHandler(error) {
    var errorMsg = error;
    if (error.statusText === "UNAUTHORIZED") {
      errorMsg = 'Não foi possível efetuar login. Por favor verifique seu nome de usuário e senha e tente novamente.'
    }
    DialogFactory.showErrorDialog(errorMsg);
  }

  function success(response) {
    if (response.data.error) {
      error(response.data.error);
    } else {
      localStorage.setItem('_unisession', Date.now());
      localStorage.setItem('_user', response.data[0].displayName);
      $state.go('dashboard.questions');
    }
  }



  function error(err) {
    DialogFactory.showErrorDialog('Credenciais invalidas');
  }

}
