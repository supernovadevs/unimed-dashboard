'use strict';
angular
	.module('login')
	.config(function ($stateProvider) {
		$stateProvider
			.state('login', {
				data : { pageTitle: 'Login' },
				url: "/login",
				controller: 'LoginController as vm',
				templateUrl: "js/components/login/views/login.html",
				resolve: {
					data: ['AuthFactory', function (AuthFactory) {
						return AuthFactory.loadSession();
					}]
				}
			});
	});
