'use strict';

angular.module('login').factory('LoginFactory', LoginFactory);

function LoginFactory($http, $base64, $q, API_ENDPOINT, $httpParamSerializer) {

  var service = {
    loginAD: loginAD,
    loginGS: loginGS,
    loginAuth: loginAuth,
    logout: logout
  };

  return service;

  function logout(id) {
    var dfd = $q.defer();
    return dfd.promise;
  }

  function loginAD(user) {
    var dfd = $q.defer();
    var userEnconded = $base64.encode(user.name + ":" + user.password);
    var configRequest = {
      method: 'GET',
      url: "https://osb.unimedfortaleza.com.br/ldap/proxy/auth",
      headers: {
        'Authorization': "Basic " + userEnconded
      }
    }
    
    $http(configRequest).then(
      function(response) {
        dfd.resolve(user);
      },
      function(error) {
        dfd.reject(error);
      })
    return dfd.promise;
  };

  function loginAuth(user) {
    var dfd = $q.defer();
    var userEnconded = $base64.encode("midiasdigitais@unimedfortaleza.com.br:midias#2015");
    var configRequest = {
      method: 'GET',
      url: API_ENDPOINT.getjwt,
      headers: {
        'Authorization': "Basic " + userEnconded
      }
    }
    
    $http(configRequest).then(function(response) {
      if(response.data.error) {
        dfd.reject('Usuário não cadastrado');
      } else {
        localStorage.setItem('gswebtoken', response.data['X-GS-JWT']);
        localStorage.setItem('_id', response.data.username);
        dfd.resolve(user);
      }
    }, function(error) {
      dfd.reject('erro ao logar no server');
    })
    return dfd.promise;
  };
  
  function loginGS(user) {
    var dfd = $q.defer();
    var query = {"query": {"username": { "$regex": user.name, "$options": "i"}}};
    var configRequest = {
      method: 'POST',
      url: API_ENDPOINT.host + 'script.admin/find',
      data: query,
      headers: {
        'Content-Type': 'application/json'
      }
    }

    $http(configRequest).then(
      function(response) {
        if (response.data.length === 0) {
          dfd.reject('Usuário Não cadastrado');
        } else {
          dfd.resolve(response);
        }
      },
      function(error) {
        dfd.reject(error);
      })
    return dfd.promise;
  }
}
