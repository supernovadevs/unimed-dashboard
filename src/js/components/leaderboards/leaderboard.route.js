'use strict';
angular
	.module('leaderboard')
	.config(function ($stateProvider) {
		$stateProvider
			.state('dashboard.leaderboards', {
				data : { pageTitle: 'Placar' },
				url: "/leaderboards",
				views: {
					'menuContent': {
						controller: 'LeaderboardController as vm',
						templateUrl: "js/components/leaderboards/views/index.html"
					}
				}
			})
	});
