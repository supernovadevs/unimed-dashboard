(function() {
	'use strict';
	angular
		.module('leaderboard', [])
		.controller('LeaderboardController', LeaderboardController);

	function LeaderboardController($state, $stateParams, DialogFactory, ApiFactory) {
		var vm = this;
		vm.leaderboards = [];
		vm.loadAll = loadAll;
		vm.currentId = localStorage.getItem('_id');
		vm.sort = {
			type: "rank",
			reverse: false
		}

		function loadAll() {
			ApiFactory.getAllScores(vm.currentId).then(function(response) {
				console.log(response);
				vm.leaderboards = response.data.map(mapItem);
				console.log(vm.leaderboards);
			})
		}

		function mapItem(item) {
			if (item.score < 400) {
				item.role = 'Aprendiz';
			} else if (item.score >= 400 && item.score < 1000) {
				item.role = 'Estagiário'
			} else if (item.score >= 1000 && item.score < 1790) {
				item.role = 'Assistente'
			} else if (item.score >= 1790 && item.score < 2940) {
				item.role = 'Analista Júnior'
			} else if (item.score >= 2940 && item.score < 4430) {
				item.role = 'Analista Pleno'
			} else if (item.score >= 4430 && item.score < 6240) {
				item.role = 'Analista Sênior'
			} else if (item.score >= 6240 && item.score < 8390) {
				item.role = 'Coordenador'
			} else if (item.score >= 8390 && item.score < 10860) {
				item.role = 'Gerente'
			} else if (item.score >= 10860 && item.score < 13670) {
				item.role = 'Superintendente'
			} else if (item.score >= 13670 && item.score < 16800) {
				item.role = 'Diretor'
			} else if (item.score >= 16800 && item.score < 24000) {
				item.role = 'Presidente'
			} else if (item.score >= 24000 && item.score < 17360) {
				item.role = 'Presidente'
			} else if (item.score >= 32640 && item.score < 50000) {
				item.role = 'Presidente (3º tíquete)'
			}
			else if (item.score >= 50000) {
				item.role = 'Presidente (4º tíquete)'
			}
			return item;
		}

	}
}());
