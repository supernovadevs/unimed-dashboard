'use strict';
angular
	.module('dashboard',[])
	.config(function ($stateProvider) {
		$stateProvider
			.state('dashboard', {
				abstract:true,
				url: "/dashboard",
        controller: 'DashboardController as vm',
				templateUrl: "js/components/dashboard/view/dashboard.view.html",
				resolve: {
					data: ['AuthFactory', function (AuthFactory) {
						return AuthFactory.verifySession();
					}]
				}
			});
	});
