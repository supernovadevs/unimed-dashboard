(function() {
	'use strict';
	angular.module('dashboard').controller('DashboardController', DashboardController);

	DashboardController.$inject = [
		'DashboardFactory',
		'$stateParams',
		'$state',
		'$location',
		'LoginFactory',
		'Popeye',
		'$compile',
		'$scope'
	];
	/* @ngInject */

	function DashboardController(DashboardFactory, $stateParams, $state, $location, LoginFactory, Popeye,$compile,$scope) {
		var vm = this;

		angular.extend(vm, {
			logout: logout,
			showModal: showModal,
			closeModal: closeModal
		});

		var htmlcontent = $('#loadhtml ');
	 htmlcontent.load('/Pages/Common/contact.html')
	 $compile(htmlcontent.contents())($scope);

		function logout() {
      Popeye.closeCurrentModal()
			localStorage.clear();
			$state.go('login');
		}

		function showModal() {
      Popeye.openModal({
				templateUrl:'./js/common/partials/modal.html',
        controller: "DashboardController as vm"
			})
		}

		function closeModal() {
			Popeye.closeCurrentModal()
		}

	}
}());
