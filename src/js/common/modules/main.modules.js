'use strict';
angular.module('core', [
	'tools',
	'factories',
	'components'
]);

angular.module('components', [
	'login',
	'admins',
	'dashboard',
	'leaderboard',
	'statistics',
	'questions',
	'promotions'
]);

angular.module('factories', [
	'apifactory',
	'auth',
	'dialog',
	'httpInterceptor'
]);

angular.module('tools', [
	'toaster',
	'base64',
	'ngAnimate',
	'ui.router',
	'ngMessages',
	'ngTagsInput',
	'720kb.datepicker',
	'pathgather.popeye',
	'unsavedChanges',
	'angularUtils.directives.dirPagination'
]);
