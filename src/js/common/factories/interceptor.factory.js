(function() {
  'use strict';
  angular
    .module('httpInterceptor', [])
    .factory('httpInterceptor', httpInterceptor);

  httpInterceptor.$inject = ['$q', '$rootScope', '$window', 'DialogFactory'];

  function httpInterceptor($q, $rootScope, $window, DialogFactory) {
    var service = {
      request: request,
      response: response,
      requestError: requestError,
      responseError: responseError
    };

    return service;

    //////////////////

    function InvalidadeSession() {
      deleteSession();
      $window.location = '#/login';
    }

    function request(config) {
    	$rootScope.showLoading = true;
      config.timeout = 10000;
      config.headers['X-GS-JWT'] = localStorage.getItem('gswebtoken') || '';
      return config;
    }

    function response(response) {
			$rootScope.showLoading = false;
      if (response.satus === 401) {
        InvalidadeSession();
      }
      return response;
    }

    function requestError(rejection) {
    	$rootScope.showLoading = false;
      DialogFactory.showErrorDialog('Ocorreu um erro tente mais tarde', 'error');
      return $q.reject(rejection);
    }

    function responseError(rejection) {
    	$rootScope.showLoading = false;
      if (rejection.status === 401) {
        InvalidadeSession();
      }
      return $q.reject(rejection);
    }


    function deleteSession() {
      localStorage.clear();
    }
  }
}());
