'use strict';

angular.module('apifactory', []).factory('ApiFactory', ApiFactory);

function ApiFactory($http, $q, API_ENDPOINT, $httpParamSerializer) {

	var service = {
		login: login,
		getAllQuestions: getAllQuestions,
    getQuestionWithText: getQuestionWithText,
		updateQuestion: updateQuestion,
		deleteQuestion: deleteQuestion,
		getquestion: getquestion,
		saveQuestion:saveQuestion,
		getAllScores:getAllScores,
		checkDailyQuestions:checkDailyQuestions
	};

	return service;

	function getAllQuestions(date) {
		var dfd = $q.defer();
    var sDate = moment(date.start, "DD-MM-YYYY").toISOString();
    var eDate = moment(date.end, "DD-MM-YYYY").toISOString();
		var filter = {query:{"formattedDate":{"$gte": sDate, "$lte": eDate}}, limit:1000};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/find',
			data: filter,
			headers: {
				'Content-Type': 'application/json'
			}
		}
		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})
		return dfd.promise;
	}
  
  function getQuestionWithText(queryText){
    var dfd = $q.defer();
		var limit = {query: {"$or": [ {"text" : {"$regex": queryText, "$options": "i"} } , {"introduction" : {"$regex": queryText, "$options": "i"} }, {"tags.text" : {"$regex": queryText, "$options": "i"} } ]}, limit:1000};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/find',
			data: limit,
			headers: {
				'Content-Type': 'application/json'
			}
		}
		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})
		return dfd.promise;
  }

	function saveQuestion(question){
		var dfd = $q.defer();
		var questionData = [question];
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/insert',
			data: questionData,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

	function getquestion(id) {
		var dfd = $q.defer();
		var question = {query: {"_id": id}};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/find',
			data: question,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

	function updateQuestion(question) {
		var dfd = $q.defer();
    delete question.mapDate;
		var question = {query: {"_id": question._id}, update: question,
    multi: false, upsert: false};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/update',
			data: question,
			headers: {
				'Content-Type': 'application/json'
			}
		}

		$http(configRequest).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

	function deleteQuestion(question) {
		var dfd = $q.defer();
		var queryContent = {
			"_id": question._id
		};
		var question = {query: queryContent};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/remove',
			data: question,
			headers: {
				'Content-Type': 'application/json'
			}
		}
		$http(configRequest).then(function(response) {
			dfd.resolve(response)
		}, function(error) {
			dfd.reject(error);
		})

		return dfd.promise;
	}

	function login(user) {
		var defer = $q.defer();
		var params = {
			"@class": ".AuthenticationRequest",
			"password": user.password,
			"userName": user.name
		}
		$http.post(API_ENDPOINT.requestAPI + 'AuthenticationRequest', params).then(function(response) {
			dfd.resolve(response);
		}, function(error) {
			dfd.reject(error);
		})
		return dfd.promise;
	}

	function getAllScores(id) {
		var dfd = $q.defer();
    
    $http.post(API_ENDPOINT.requestAPI + 'LogEventRequest', {
        "@class": ".LogEventRequest",
        "eventKey": "LeaderboardWithScriptDataRequest",
        "playerId": "58309640a1712a04a8cd913f",
        "entryCount": 999999,
        "includeFirst": 0,
        "includeLast": 0,
        "offset": 0
    }
    ).then(function(response) {
      console.log(response);
      dfd.resolve(response.data.scriptData.leaderboardData);
    }, function(error) {
      dfd.reject(error);
    })
    return dfd.promise;
  }


	function checkDailyQuestions(id, date) {
    var dfd = $q.defer();
		var question = {query: {"date": date}};
		var configRequest = {
			method: 'POST',
			url: API_ENDPOINT.host + 'script.question/find',
			data: question,
			headers: {
				'Content-Type': 'application/json'
			}
		}
    
    $http(configRequest).then(function(response) {
      if (response.data.length < 2) {
        dfd.resolve(response);
      }else {
      	dfd.reject('Numero máximo de perguntas cadastradas para esse dia');
      }
    }, function(error) {
      dfd.reject(error);
    })
    return dfd.promise;
  }
}
