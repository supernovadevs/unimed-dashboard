'use strict';

angular
	.module('dialog', [])
	.factory('DialogFactory', DialogFactory);

function DialogFactory(toaster) {
	var service = {
		showSuccessDialog: showSuccessDialog,
		showErrorDialog: showErrorDialog
	};

	return service;

	function showSuccessDialog(message, type) {
		message = message || 'Ação Realizada com sucesso';
		type = type || 'success';
		toaster.pop(type, 'Sucesso', message);
	}

	function showErrorDialog(message, type) {
		message = message || 'Não foi possivel realizar essa ação';
		type = type || 'error';
		toaster.pop(type, "Erro", message);
	}

	function showInfoDialog(message, type) {
		message = message || 'Ação Realizada com sucesso';
		type = type || 'info';
		toaster.pop('success', "Salvo", "Cadastro Realizado com sucesso");
	}

	function showDialog(message, type) {
		message = message || 'Ação Realizada com sucesso';
		type = type || 'Sucess';
		toaster.pop('success', "Salvo", "Cadastro Realizado com sucesso");
	}

}
