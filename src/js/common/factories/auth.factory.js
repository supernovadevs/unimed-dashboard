'use strict';
angular
	.module('auth', [])
	.factory('AuthFactory', AuthFactory);

function AuthFactory($q, $state, $location,$window) {
	var service = {
		loadSession: loadSession,
		verifySession: verifySession
	};

	return service;

	function loadSession() {
		var defer = $q.defer();
		if (localStorage.getItem('_unisession') !== null) {
			$location.path('dashboard/questions');
		}
		defer.resolve();
		return defer.promise;
	}

	function verifySession(url) {
		console.log(localStorage.getItem('_unisession') === null);
		var defer = $q.defer();
		if (localStorage.getItem('_unisession') === null && url !== '/login') {
			defer.reject($window.location = '#/login');
		}
		defer.resolve();
		return defer.promise;
	}
}
